#pragma once
#include <iostream>
#include <cstdlib>
#include <string>
#include "UnorderedArray.h"


int binarySearch(int begin, int last, int searchValue)
{
	if (begin <= last)
	{
		int midpoint = (begin + last) / 2;
		if (midpoint == searchValue) return midpoint;

		if (midpoint > searchValue) return binarySearch(begin, midpoint - 1, searchValue);

		if (midpoint < searchValue) return binarySearch(begin, midpoint + 1, searchValue);
	}
	return -1;
}