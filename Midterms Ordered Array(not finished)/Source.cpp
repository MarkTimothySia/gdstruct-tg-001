#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

UnorderedArray<int> mgaNumbers(1);

/*Checklist
make Unoreded Array like this
number - number - number
make Ordered Array like this
lowest - mid - highest
each number must be added to the ordered array
*/

void printUnordered()
{
	cout << "Unordered Array" << endl;
	for (int counter = 0; counter < mgaNumbers.getSize(); counter++)
	{
		cout << mgaNumbers[counter]<<" - ";
	}
	cout << endl << endl;
}

int ordered(int& number)
{
	int result = binarySearch(mgaNumbers.getSize - 1, mgaNumbers.getSize, result);

	return result;
}

void printOrdered(int& number)
{
	cout << "Ordered Array" << endl;
	int orderedArray[mgaNumbers.getSize];

	for (int counter = 0; counter < mgaNumbers.getSize; counter++)
	{
		number = orderedArray[counter];
		cout << number <<" - ";
	}
}


void main()
{
	int added;
	int number;
	do
	{
		cout <<"Add numbers" << endl;
		cin >> added;
		mgaNumbers.push(added);
		printUnordered();
		number = added;
		number = ordered(number);
		printOrdered(number);
	} while (mgaNumbers.getSize != 10);

}