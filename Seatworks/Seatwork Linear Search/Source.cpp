#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void print(UnorderedArray<int>grades);

void search(UnorderedArray<int>grades, int& searchValue)
{
	cout << "Linear Search put a value" << endl;
	cin >> searchValue;
	int result = linearSearch(grades, searchValue);
	if (result <= 10)
	{
		cout << "Number has been found: " << searchValue << endl;
	}
	else
	{
		cout << "Value not found " << endl;
	}
}

void actions(UnorderedArray<int>grades)
{
	int choice,input,searchValue;
	cout << "1. Push back a new variable" << endl << "2. Search a variable" << endl << "3. Exit program" << endl;
	cin >> choice;

	system("cls");
	switch (choice)
	{
	case 1:
		cout << "Input new number" << endl;
		cin >> input;
		grades.push(input);
		actions(grades);
		break;
	case 2:
		search(grades, searchValue);
		actions(grades);
		break;
	case 3:
		exit(1);
		break;
	default:
		cout << "No actions given please try again" << endl;
		actions(grades);
		break;
	}

}

void print(UnorderedArray<int>grades)
{
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}
}

void main()
{
	UnorderedArray<int> grades(5);
	int searchValue;
	
	for (int i = 1; i <= 10; i++){
		grades.push(i);
	}
	
	for (int i = 0; i < grades.getSize(); i++){
		cout << grades[i] << endl;
	}

	for (int i = 0; i < grades.getSize(); i++) {
		print(grades[i]);
	}

	while (1)
	{
		actions(grades);
	}

	
	system("pause");
	
}

//Binary serach O(logN), Linear Search O(N)