#pragma once
#include<assert.h>

template<class T>

//Assert -> feed a condition (like bool) if true then mag pupush

class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1):mArray(NULL),mMaxSize(0),mGrowSize(0),mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			//allocate new Memory
			mArray = new T[mMaxSize];
			//memory allocated -> size of T * maxSize
			memset(mArray, 0, sizeof(T) * mMaxSize);

			// ? internally operator if or else statement
			//? = if : = else
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}
	
	virtual ~UnorderedArray()
	//deletes usseless stuff the array itself
	{
		delete[] mArray;
		mArray = NULL;
	}

	virtual void push(T value)
	// any type of array will be handled by the function
	{
		assert(mArray != NULL);
		//feed a condition (like bool) if true then it will push

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	// cout the operator automatically
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		//it will remove the last element but at a EMPTY SLOT allocated in RAM
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);
		if (index >= mMaxSize) //index is more than current size
			return;

		for (int i = index; i < mMaxSize - 1; i++) //counts same as index size 
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize) //allocated
			mNumElements = mMaxSize;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	//expands the memory
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];
		
		//CHECKS is temp has allocated memory
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		// you still add new stuff
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}

};