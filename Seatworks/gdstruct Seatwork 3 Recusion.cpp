#include <conio.h>
#include <iostream>
#include <string>

using namespace std;

//SeatWork 3

/*       -   Notes  -
void printDecreasing(int from)
{
	if (from <= 0) return;

	cout << from << " ";

	printDecreasing(from - 1);
}

int recursion(int param)
{
	if (param < 1) return 0;


	return recursion(param - 1);//caller and param -1
}
*/

/* - OLD -
void fibonacciNumber(int term, int var1 = 0, int var2 = 1, int sumTerm)
{
	if (term == 1)
	{
		cout << " " << var1;
		return;
	}
	if (term == 2)
	{
		cout << var2 << " ";
		return;
	}

	sumTerm = var1 + var2;
	var1 = var2;
	var2 = sumTerm;

	fibonacciNumber(term, var1, var2, sumTerm);
}
*/

void addition(int lo, int hi, int sum, int& copy)
{
	if (lo != 0 && lo > hi) return;

	if (lo < hi)
	{
		cout << lo << " + ";
	}
	else if (lo >= hi)
	{
		cout << lo << " = ";
	}

	sum = lo + sum;
	copy = sum;
	addition(lo + 1, hi, sum, copy);
}

int fibNumber(int term);

void printFibSequence(int term, int var1, int var2, int nextTerm)
{
	if (nextTerm <= fibNumber(term))
	{
		cout << nextTerm << ", ";

		var1 = var2;
		var2 = nextTerm;
		nextTerm = var1 + var2;
		printFibSequence(term, var1, var2, nextTerm);
	}
}

int fibNumber(int term)
{
	if (term <= 1) return term;

	return fibNumber(term - 1) + fibNumber(term - 2);
}

void primeChecker(int i, int term)
{
	if (i >= 5)return;

	//divisible by 2 starting point of the checking
	bool prime = true;

	if (fibNumber(term) % 2 == 0)
	{
		prime = false;
		cout << fibNumber(term) << " is divisible by " << 2 << endl;

	}
	else if (fibNumber(term) % 3 == 0)
	{
		prime = false;
		cout << fibNumber(term) << " is divisible by " << 3 << endl;

	}
	else if (fibNumber(term) % 5 == 0)
	{
		prime = false;
		cout << fibNumber(term) << " is divisible by " << 5 << endl;

	}
	else if (fibNumber(term) % 7 == 0)
	{
		prime = false;
		cout << fibNumber(term) << " is divisible by " << 7 << endl;

	}
	else if (fibNumber(term) % 11 == 0)
	{
		prime = false;
		cout << fibNumber(term) << " is divisible by " << 11 << endl;

	}

	if (prime)
	{
		cout << "There are no factors, so this must be a prime number" << endl;
		return;
	}
	else
	{
		cout << "This number has factors that are divisible so this is not a prime number" << endl;
		return;
	}

	primeChecker(++i, term);

	/*
	if (prime)
	{
		cout << fibNumber(term) << " is a Prime Number" << endl;
	}
	else cout << fibNumber(term) << " is not a Prime Number" << endl;
	*/
}


int main()
{
	//Sum of Digits Variables
	int lo, hi, copy, term;
	int sum = 0;

	//Fibonacci Variables
	int var1 = 0;
	int var2 = 1;
	int nextTerm = 0;

	//Prime Number
	int i = 2;

	//-  Number 1
	cout << "Sum of Digits " << endl;
	cout << "input the lowest number" << endl;
	cin >> lo;

	cout << "input the higest number" << endl;
	cin >> hi;

	addition(lo, hi, sum, copy);
	cout << copy << endl;

	system("pause");
	system("cls");

	//-  Number 2
	cout << "Fibonacci Number" << endl;
	cout << "Input the number of counts in the fibonacci sequence" << endl;
	cin >> term;
	cout << "Fibonacci Number: " << fibNumber(term) << endl;
	cout << "Fibonacci Sequence: ";
	printFibSequence(term, var1, var2, nextTerm);
	cout << endl;
	system("pause");
	system("cls");

	//-  Number 3
	cout << "Prime of not" << endl;
	primeChecker(i, term);



	return 0;
}