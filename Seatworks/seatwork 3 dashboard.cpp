#include <iostream>
#include <string>
#include <conio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

/*
Checklist Done
- Declare a name for the guild (user input)
- Declare an initial size for the guild (user input)
- Input the usernames of the initial members (user input)
- Print every member
- Renaming a member
- Adding a member
- Deleting a member [EMPTY SLOT]
*/

void guildName(string& gName)
{
	cout << "Please in put a Guild Name" << endl;
	getline(cin, gName);
	cout << gName << endl;
}

void actions(vector<string>& members, string& gName)
{
	int choice = 0, number;
	string name;
	string emptySlot = "Empty Slot";

	cout << "1. Print all members" << endl << "2. Rename a member" << endl << "3. Add a new member" << endl << "4. Delete a member" << endl << "5. Exit the Program" << endl;
	cin >> choice;
	switch (choice)
	{
	case 1:
		system("cls");
		cout << "Guild Name: " << gName << endl << "Members total: " << members.size() << endl;
		for (int counter = 0; counter < members.size(); counter++)
		{
			cout <<counter + 1<<". "<< members[counter] << endl;
		}
		cout << endl << endl;
		system("pause");
		actions(members, gName);
		break;

	case 2://Rename
		system("cls");
		for (int counter = 0; counter < members.size(); counter++)
		{
			cout << counter + 1 << ". " << members[counter] << endl;
		}
		cout << "Rename who?" << endl;
		cin >> number;
		cout << "What name?" << endl;
		cin >> name;
		//emplace changes the value
		members.emplace(members.begin() + number, name);
		members.erase(members.begin() + number - 1, members.begin() + number);

		actions(members, gName);
		break;
	case 3:
		cout << "New member name" << endl;
		cin >> name;
		members.push_back(name);
		cout << endl << endl;
		system("pause");
		actions(members,gName);
		break;

	case 4://Delete
		system("cls");
		
		for (int counter = 0; counter < members.size(); counter++)
		{
			cout << counter + 1 << ". " << members[counter] << endl;
		}
		cout << "Delete who?" << endl;
		cin >> number;

		//starts at picked number then goes back 1 number to start killing variables-> the picked number
		members.erase(members.begin() + number - 1, members.begin() + number);

		actions(members, gName);
		break;
	case 5:
		exit(1);
		break;
	default:
		cout << "No correct input, try again" << endl;
		cout << endl << endl;
		system("pause");
		actions(members,gName);
		break;
	}
	
}

void initializeMembers(vector<string>& members)
{
	string name,gName;
	
	cout << "Initialize the members first to be added" << endl;
	members.resize(1);

	for (int counter = 0; counter != 5; counter++)
	{
		cout << "Member " <<counter + 1<<" 's name"<< endl;
		cin >> name;
		members.push_back(name);
		if (counter == 5)
		{
			break;
		}
	}
	members.erase(members.begin());

	actions(members, gName);
	
}

int main()
{
	string gName;
	guildName(gName);
	vector<string>members;

	initializeMembers(members);
	
	while (1);
	{
		actions(members,gName);
		system("pause");
	}

	return 0;
}

